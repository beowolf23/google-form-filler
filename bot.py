import requests
import sys
from bs4 import BeautifulSoup


def submit_form(form_url, form_data):
    """
    Submits data to a Google Form using a POST request.

    Args:
        url (str): The URL of the Google Form.
        form_data (dict): Dictionary containing form field names as keys and their respective values.

    Returns:
        bool: True if the form submission was successful, False otherwise.
    """
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
    }

    try:
        response = requests.post(form_url, headers=headers, data=form_data)
        response.raise_for_status()  # Raise an exception for HTTP errors (status code other than 2xx)
        if response.status_code == 200:
            print("Form submitted successfully.")
            return True
        else:
            print(f"Failed to submit the form. Status code: {response.status_code}")
            return False
    except requests.RequestException as e:
        print(f"An error occurred: {e}")
        return False

def get_form_entry_ids(form_url):
    """
    Fetches the Google Form page and extracts all entry.<id> elements.
    Args:
        form_url (str): The URL of the Google Form.
    Returns:
        dict: A dictionary with entry.<id> keys and None values.
    """
    # Perform GET request to fetch the form page
    response = requests.get(form_url)

    # Check if the request was successful
    if response.status_code != 200:
        raise Exception(f'Failed to fetch the form page. Status code: {response.status_code}')

    # Parse the HTML response using BeautifulSoup
    soup = BeautifulSoup(response.text, 'html.parser')

    # Find all input elements with name attribute containing 'entry.'
    entry_inputs = soup.find_all('input', attrs={'name': lambda x: x and 'entry.' in x})

    # Extract the entry IDs and store them in a dictionary
    entry_list = []
    for input_element in entry_inputs:
        name = input_element['name']
        entry_id = name.split('_sentinel')[0]
        entry_list.append(entry_id)

    return entry_list

def get_command_arguments():

    # Check if the user has provided the required command-line arguments
    if len(sys.argv) < 3:
        print("Usage: python3 bot.py <form_url> <path_to_csv>")
        sys.exit(1)
    
    # Extract the form URL and CSV file path from the command-line arguments
    form_url = sys.argv[1]
    csv_path = sys.argv[2]

    return form_url, csv_path

def urls(form_url):
    """
    Extracts the form URL for GET and POST requests.

    Args:
        form_url (str): The URL of the Google Form.

    Returns:
        tuple: A tuple containing the form URL for GET and POST requests.
    """

    form_url_GET = form_url
    form_url_POST = form_url.replace('/viewform', '/formResponse')
    return form_url_GET, form_url_POST

def create_form_data(entry_ids, row):
    """
    Creates a list of tuples containing form field names and their respective values.

    Args:
        entry_ids (dict): A dictionary containing entry.<id> keys and None values.
        row (list): A list containing the values to be submitted to the form.

    Returns:
        list: A list of tuples containing form field names as keys and their respective values.
    """
    
    form_data = []

    for entry_id, value in zip(entry_ids, row):
        form_data.append((entry_id + '_sentinel', ''))
        
        if value:
            value = value.strip('[]')  
            if "|" in value:
                value = value.split("|")
                for v in value:
                    form_data.append((entry_id, v))
            else: 
                form_data.append((entry_id, value))


    return form_data

def read_csv(csv_path):
    """
    Reads data from a CSV file.

    Args:
        csv_path (str): The path to the CSV file.

    Returns:
        list: A list of lists containing the data read from the CSV file.
    """
    data = []
    try:
        with open(csv_path, 'r') as file:
            for line in file:
                data.append(line.strip().split(','))
    except FileNotFoundError:
        print(f"File not found: {csv_path}")
        sys.exit(1)

    return data

def main():
    # Get the form URL and CSV file path from the command-line arguments
    form_url, csv_path = get_command_arguments()

    form_url_get, form_url_post = urls(form_url)

    # Get the entry IDs from the form page
    entry_ids = get_form_entry_ids(form_url_get)

    # Read the data from the CSV file
    data = read_csv(csv_path)

    # Submit the form for each row in the CSV file
    for row in data:
        form_data = create_form_data(entry_ids, row)
        print(form_data)
        submit_form(form_url_post, form_data)

if __name__ == "__main__":
    main()